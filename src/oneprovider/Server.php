<?php

namespace zarlo\oneprovider;

class Server {

    /**
    * servers ID
    *
    * @var int
    */
    public $server_id;
    /**
    * Main IP
    *
    * @var string
    */
    public $ip_addr;
    /**
    * Hostname
    *
    * @var string 
    */    
    public $hostname; 
    /**
    * Status
    *
    * @var string 
    */   
    public $status;
    /**
    * and arrary or string of IP's
    *
    * @var string|string[]|null
    */
    public $ips;
    /**
    * username
    *
    * @var string 
    */   
    public $username;
    /**
    * location
    *
    * @var string 
    */       
    public $location;
    /**
    * the next due date
    *
    * @var string
    */    
    public $next_due_date;
    /**
    * billing cycle
    *
    * @var string 
    */    
    public $billing_cycle;
    /**
    * recurring_amount
    *
    * @var float 
    */    
    public $recurring_amount;
    /**
    * cancel_reason
    *
    * @var string|null
    */    
    public $cancel_reason;
    /**
    * cancel_type
    *
    * @var string|null
    */    
    public $cancel_type;

    /**
    * p
    *
    * @var oneprovider
    */   
    private $p;

    /**
    * reverse_modify
    *
    * @param string $address
    * @param string $reverse
    * @return bool
    */ 
    public function reverse_modify($address, $reverse)
    {

        $res = $this->p->call_api("POST", "/server/action/", [], [ "server_id" => $this->server_id, "action" => "reverse_modify", "address" => $address, "reverse" => $reverse ]);

        if($res["result"] == "success") return true;

        return false;

    }

    /**
    * reboot the server
    *
    * @param string $reason
    * @return bool
    */ 
    public function reboot($reason = "unknown")
    {

        $res = $this->p->call_api("POST", "/server/action/", [], [ "server_id" => $this->server_id, "action" => "reboot", "reason" => $reason ]);

        if($res["result"] == "success") return true;

        return false;

    }

    /**
    * reinstall
    *
    * @param int $os_id
    * @param string $hostname
    * @param string $username
    * @param string $password
    * @return bool
    */ 
    public function reinstall($os_id, $hostname, $username, $password)
    {

        $res = $this->p->call_api("POST", "/server/action/", [], [ "server_id" => $this->server_id, "action" => "reinstall", "os_id" => $os_id, "hostname" => $hostname, "username" => $username, "password" => $password ]);

        if($res["result"] == "success") return true;

        return false;

    }

    /**
    * get_failovers
    *
    * @return bool|IP[]
    */ 
    public function get_failovers()
    {

        $res = $this->p->call_api("POST", "/server/action/", [], [ "server_id" => $this->server_id, "action" => "get_failovers" ]);

        if($res["result"] == "success")
        {

            $ip_json = $res["response"]["failovers"];

            $output = [];

            foreach($ip_json as $ip)
            {

                $temp = new IP();

                $temp->address = $ip["ip"];
                $temp->status = $ip["status"];
                $temp->mac = $ip["mac"];

                array_push($output, $temp);

            }

            return $output;


        }

        return false;

    }

    /**
    * get_power_status
    *
    * @return bool|null
    */ 
    public function get_power_status()
    {

        $res = $this->p->call_api("POST", "/server/action/", [], [ "server_id" => $this->server_id, "action" => "get_failovers" ]);

        if($res["result"] == "success")
        {

            if($res["response"]["server_info"]["power"] === "ON") return true; else return false; 

        }

        return null;        

    }

    /**
    * cancel_server
    *
    * @param string $reason
    * @param bool $immediate = false
    *
    * @return bool
    */ 
    public function cancel_server($reason, $immediate = false)
    {

        $res = $this->p->call_api("POST", "/server/cancel/", [], [ "server_id" => $this->server_id, "reason" => $reason, "type" => $immediate? "Immediate"   : "End of Billing Period" ]);

        if($res["result"] == "success") return true;

        return false;       

    }

    /**
    * remove_cancellation
    *
    * @return bool
    */ 
    public function remove_cancellation()
    {

        $res = $this->p->call_api("POST", "/server/remove/cancellation/", [], [ "server_id" => $this->server_id ]);

        if($res["result"] == "success") return true;

        return false;       

    }

    /**
    * set_hostname
    *
    * @param string $hostname
    *
    * @return bool
    */ 
    public function set_hostname($hostname)
    {

        $res = $this->p->call_api("POST", "/server/hostname/", [], [ "server_id" => $this->server_id, "hostname" => $hostname ]);

        if($res["result"] == "success") return true;

        return false;       

    }

    public function __construct($p)
    {
        $this->p = $p;


    }

}