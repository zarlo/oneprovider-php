<?php
namespace zarlo\oneprovider;

use zarlo\oneprovider\Server;

class oneprovider {

    public function call_api($http_method, $endpoint, $get = array(), $post = array()) {  
        if (!empty($get))  
            $endpoint .= '?' . http_build_query($get);  
     
        $call = curl_init();  
        curl_setopt($call, CURLOPT_URL, 'https://api.oneprovider.com' . $endpoint);  
        curl_setopt($call, CURLOPT_HTTPHEADER, array('Api-Key: ' . $this->api_key, 'Client-Key: ' . $this->client_key, 'X-Pretty-JSON: 1'));  
        curl_setopt($call, CURLOPT_RETURNTRANSFER, true);  
     
        if ($http_method == 'POST') {  
            curl_setopt($call, CURLOPT_POST, true);  
            curl_setopt($call, CURLOPT_POSTFIELDS, http_build_query($post));  
        }  
        elseif ($http_method == 'DELETE') {  
          curl_setopt($call, CURLOPT_CUSTOMREQUEST, $http_method);  
        }  
     
        $result = curl_exec($call);  
        return $result;  
    }  

    private $api_key, $client_key;

    /**
    * Create new oneprovider API client
    *
    * @param string $client_key
    * @param string $api_key
    */
    public function __construct($client_key, $api_key)
    {
        $this->api_key = $api_key;
        $this->client_key = $client_key;

    }

    /**
     * 
     * get all servers
     * 
     * @return server[]
     */
    public function get_Servers()
    {

        $servers = $this->call_api("GET", "/server/list/")["response"]["servers"];

        $output = [];

        foreach($servers as $server)
        {

            $temp = new Server($this);
            $temp->server_id = $server["server_id"];
            $temp->ip_addr = $server["ip_addr"];
            $temp->hostname = $server["hostname"];
            $temp->status = $server["status"];
            $temp->ips = $server["ips"];
            $temp->username = $server["username"];
            $temp->location = $server["location"];
            $temp->next_due_date = $server["next_due_date"];
            $temp->billing_cycle = $server["billing_cycle"];
            $temp->recurring_amount = $server["recurring_amount"];
            $temp->cancel_reason = $server["cancel_reason"];
            $temp->cancel_type = $server["server_id"];
            $temp->server_id = $server["cancel_type"];

            array_push($output, $temp);

        }

        return $output;

    }

    /**
     * 
     * get all servers
     * 
     * @param int $id
     * 
     * @return server[]
     */
    public function get_Server($id)
    {

        $server = $this->call_api("GET", "/server/info/" . $id)["response"];

        $temp = new Server($this);
        $temp->server_id = $server["server_id"];
        $temp->ip_addr = $server["ip_addr"];
        $temp->hostname = $server["hostname"];
        $temp->status = $server["status"];
        $temp->ips = $server["ips"];
        $temp->username = $server["username"];
        $temp->location = $server["location"];
        $temp->next_due_date = $server["next_due_date"];
        $temp->billing_cycle = $server["billing_cycle"];
        $temp->recurring_amount = $server["recurring_amount"];
        $temp->cancel_reason = $server["cancel_reason"];
        $temp->cancel_type = $server["server_id"];
        $temp->server_id = $server["cancel_type"];


        return $temp;

    }
    
}