<?php
namespace zarlo\oneprovider;

class IP {

    public $address = null;
    public $type = null;
    public $status = null;
    public $reverse = null;
    public $mac = null;
    public $switch_port_state = null;

}